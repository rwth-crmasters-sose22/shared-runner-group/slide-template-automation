---
title: Shotcrete Automation 
description: Utilization of robotic arms in shotcrete applications
author: Ali Delfani, Somayyeh Ekraahmadsani
keywords: shotcrete,robotic arm,automation
url: https://rwth-crmasters-sose22.gitlab.io/shared-runner-group/slide-template-automation/#1
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
---

# Utilization of Robotic Arms in Shotcrete Applications 
## An effective tool for optimal performance
  
---
 
<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .9 em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>  

# Checklists of our journey


- [x] _==&leftarrow; Finding locked concept segment==_{.float-right}Done   


- [x] _==&leftarrow; Research on open source repositories segment==_{.float-right}Done    


- [ ] _==&leftarrow; Look for expert opinion segment==_{.float-right}Under negotioation   


- [ ] _==&leftarrow; Prototyping and documentation segment==_{.float-right}Still to be done

---
# What is shotcrete and when is it used?


> "Shotcrete is a method of applying concrete projected at high velocity primarily on to a vertical or overhead surface".
>
> "Shotcrete is applied using a wet- or dry-mix process. The wet-mix shotcrete process mixes all ingredients, including water, before introduction into the delivery hose. The dry-mix shotcrete process adds water to the mix at the nozzle. Shotcrete is used in new construction and repairs and is suitable for curved and thin elements".
---
  

# Shotcrete applications:

* ### Ground Support & Underground
    * Stabilization
    * Soil Nailing
    * Tunneling
    * Mining  

* ### Repair & Restoration
   * Bridges
   * Parking Garages
   * Dams & Reservoirs
   * Seismic Retrofit
   * Marine
   * Sewers  
     
---
  
  <img src="https://bestsupportunderground.com/wp-content/uploads/2015/05/shotcrete-equipment-concrete-spraying-equipment.jpg" width="1050" height="600" /> 


---

<img src="https://sikaconcrete.co.uk/wp-content/uploads/2017/05/sika-shotcrete-application-underground.jpg" width="1050" height="600" /> 

---
 
<img src="https://sikaconcrete.co.uk/wp-content/uploads/2017/05/sika-shotcrete-exterior-application-1024x682.jpg" width="1050" height="600" /> 
 
---  


# Open source repositiries

- [path planning of robotic arms](https://github.com/abr/abr_control);   

- [control of a robot arm  plane](https://github.com/rleddy/Robot-Arm-2D);     

- [2 DoF robot arm simulation in a 2D space](https://github.com/zaraanry/2D-Path-Planning);    

- [N joint arm to a point control simulation:](https://github.com/AtsushiSakai/PythonRobotics#arm-navigation)   

---

# Mindmap*


::: fence
@startuml

@startmindmap
*[#Orange] Automation of construction machinery  
**[#Orange] Excavation & initial stabilization scope  
*** TBM
*** Excavator
*** Loader
*** Truck
***[#Orange] Shotcrete machine
**** Manual operation
***** Hazards to workforce
****** Excessive dust in the air
******* Diseases
****** Yet unstabled zone
******* Life threatening situation
****[#Orange] Semi-autonomous operation
*****[#Orange] A robotic arm instead of a remote controller
******[#Orange] A programmable robotic arm following instruction (speed/path)
*******[#Orange] Minimum hazard to human
*******[#Orange] High efficiency
**** Fully-autonomous operation
***** Out of the scope of DDP class

@endmindmap
@enduml
:::

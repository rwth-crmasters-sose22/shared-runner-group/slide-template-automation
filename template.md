---
title: Please change this with the title of your presentation
description: Please change this with a one-line description of your presentation
author: Please replace this with your name(s)
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
---

# DDP Concept : Shotcrete Automation 
## It will crush the world
  
---
 
<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .9 em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>  

# Checklists of our journey


- [x] _==&leftarrow; Finding locked concept segment==_{.float-right}Done   


- [x] _==&leftarrow; Research on open source repositories segment==_{.float-right}Done    


- [ ] _==&leftarrow; Look for expert opinion segment==_{.float-right}Under negotioation   


- [ ] _==&leftarrow; Prototyping and documentation segment==_{.float-right}Still to be done

---
# What is shotcrete and when is it used?


> 
> "Shotcrete is a method of applying concrete projected at high velocity primarily on to a vertical or overhead surface".
>
> "Shotcrete is applied using a wet- or dry-mix process. The wet-mix shotcrete process mixes all ingredients, including water, before introduction into the delivery hose. The dry-mix shotcrete process adds water to the mix at the nozzle. Shotcrete is used in new construction and repairs and is suitable for curved and thin elements".


Paragraphs get to the point :) (check emoji textual representations [here](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/shortcuts.js))

---
# Shotcrete applications:

* ### Ground Support & Underground
    * Stabilization
    * Soil Nailing
    * Tunneling
    * Mining  

* ### Repair & Restoration
   * Bridges
   * Parking Garages
   * Dams & Reservoirs
   * Seismic Retrofit
   * Marine
   * Sewers  
     
---
<img src="https://sikaconcrete.co.uk/wp-content/uploads/2017/05/sika-shotcrete-exterior-application-1024x682.jpg" width="400" height="300" /> 


---  


# Open source repositiries

- [path planning of robotic arms](https://github.com/abr/abr_control);   

- [control of a robot arm  plane](https://github.com/rleddy/Robot-Arm-2D);     

- [2 DoF robot arm simulation in a 2D space](https://github.com/zaraanry/2D-Path-Planning);    

- [N joint arm to a point control simulation:](https://github.com/AtsushiSakai/PythonRobotics#arm-navigation)   

---

# Mindmaps

But also, any **plantuml** diagram...

::: fence
@startuml

@startmindmap
* Excavation & Initial stabilization 
** Manual operating of existing machinery
*** Hazards to workforce
**** Air pollution (Tunnel)
***** Diseases
**** Unstable zone
***** Life threatening situation on site
*** Low efficiency
**** Waste of budget
***** Time consuming 
****** Slow pace of operation
** process/software automation
*** Semi-autonomous machinery
**** Shotcrete operating
*****  **Robotic arm instead of remote controller**
****** No hazard to human workforce
****** High efficiency
****** Budget saving
****** Time saving
****** Fast pace of operation
** Self driving robotic
 
@endmindmap
@enduml
:::
